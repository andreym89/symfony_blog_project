<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\NewsRepository")
 */
class News
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, unique=true)
     */
    private $title;

    /**
     * @ORM\Column(type="text")
     */
    private $description;

    /**
     * @ORM\Column(type="datetime")
     */
    private $date_updated;

    public function getId()
    {
        return $this->id;
    }

    public function getTitle()
    {
        return $this->title;
    }

    public function setTitle(string $title)
    {
        $this->title = $title;

        return $this;
    }

    public function getDescription()
    {
        return $this->description;
    }

    public function setDescription(string $description)
    {
        $this->description = $description;

        return $this;
    }

    public function getDateUpdated()
    {
        return $this->date_updated;
    }

    /*public function setDateUpdated(DateTimeInterface $date_updated) //не используем т к в конструкторе указываем значение для даты_апдейт
    {
        $this->date_updated = $date_updated;

        return $this;
    }
     */
    
    public function __construct() { //когда создается объект класса запускается в первую очередь конструктор
        $this->date_updated = new \DateTime('now'); //текущее время сохраняем в дату_апдейт
    }
}
