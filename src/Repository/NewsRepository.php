<?php

namespace App\Repository;

use App\Entity\News;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method News|null find($id, $lockMode = null, $lockVersion = null)
 * @method News|null findOneBy(array $criteria, array $orderBy = null)
 * @method News[]    findAll()
 * @method News[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class NewsRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, News::class);
    }
    public function addNews($data){ //$data - то что мы получаем из форм
        
        //$em менеджер
        $em = $this->getEntityManager(); //получение обьекта по работе с базой данных
            
        $News = new News();  //\App\Entity\News(); //создаем экземляр обьекта класса News 
           
            
        //задаем параметры новости
        $News->setTitle($data['title']);
        $News->setDescription($data['description']);
        //дату добавления в классе News.php
                     
        $em->persist($News);// подготавливает запрос
        
        $em->flush(); //создаем транзу для запросов
        return true;
        
    }
    public function getNews(){
        
        //$entity manager менеджер
        $em = $this->getEntityManager(); //получение обьекта по работе с базой данных
            
        //$query = $em->createQuery('SELECT n.id, n.title, n.description, n.date_updated FROM App\Entity\News');
        $query = $em->createQuery('SELECT n FROM App\Entity\News n ORDER BY n.id DESC');
        
        return $query->getResult();
        
    }
    
}
