<?php

namespace App\Controller; // app - папка src(с него идет поиск), по неймсейсу система понимает где искать файл

use \Symfony\Bundle\FrameworkBundle\Controller\AbstractController;// as AbstractController; //as можно не писать исполбзуется посл часть пути как имя


class AboutContactsController extends AbstractController
{
    public function contacts()
    {
        return  $this->render('contacts.html.twig');//render функция ищет уже в папке template файл contacts
    }
    public function about()
    {
        return  $this->render('about.html.twig');//render функция ищет уже в папке template файл about
    }
}