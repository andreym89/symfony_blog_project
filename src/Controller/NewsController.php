<?php

namespace App\Controller; // app - папка src(с него идет поиск), по неймсейсу система понимает где искать файл

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;// as AbstractController; //as можно не писать исполбзуется посл часть пути как имя
use Symfony\Component\HttpFoundation\Request; //добавляем чтобы работать с реквестами,  алиасаом будет посл слово пути (тут Реквест)
use App\Entity\News;


class NewsController extends AbstractController
{
    public function showList()  //будет выводить список новостей
    {
        $em = $this->getDoctrine()->getManager();
        $NewsRepo = $em->getRepository(News::class); //    \неймспейс из шапки файла
        
        
        $news_list = $NewsRepo->getNews();
        
        //dump($news); //аналог вардампа в пхп
        
        return  $this->render('news/list.html.twig', [
            'news_list' => $news_list
        ]);//render функция ищет уже в папке template
    }
    
    public function add(Request $req) //добавляем еще ответ, в аргументы передаем объект из подключаемого класса 
    {
        //var_dump($req->request->all()); //запрашиваем все( all(); ) параметры из обьекта request из переменной req
        if($req->isMethod('POST'))
        {
            $data = $req->request->all(); //в дату попадает запрос
            
            $em = $this->getDoctrine()->getManager();
            
            $NewsRepo = $em->getRepository(News::class); //    \неймспейс из шапки файла
            
            
            try {
                //помещаем код который может вызвать в ошибку в трай
                $NewsRepo->addNews($data); // передаем данные в невсРепозиторий 
               
            } catch (\Doctrine\DBAL\DBALException $ex) {
                var_dump("Error");
                return $this->render('news/exists.html.twig');
            }
             
            /*
            $em = $this->getDoctrine()->getManager(); //получение обьекта по работе с базой данных
            
            $News = new \App\Entity\News(); //создаем экземляр обьекта класса News 
            
            
            //задаем параметры новости
            $News->setTitle($data['title']);
            $News->setDescription($data['description']);
            //дату добавления в классе News.php
            
            
            $em->persist($News);// подготавливает запрос
            $em->flush(); //создаем транзу для запросов
            */
            
            return  $this->render('news/success.html.twig'); //возвращаем страницу успеха
        }
        return $this->render('news/add.html.twig',[
            'title' => ''
        ]);
    }
    
    public function show($id) //аналог функции add 
    { 
        
        $em = $this->getDoctrine()->getManager(); //entity manager может вытаскивать по одной строке по айди

        $News = $em->find(News::class, $id);
        
        //dump($News); //die;
        
        return $this->render('news/show.html.twig', [
            'News' => $News
        ]);
    }
    public function deleteNews($id) //аналог функции add 
    { 
        
        $em = $this->getDoctrine()->getManager(); //entity manager может вытаскивать по одной строке по айди

        $News = $em->find(News::class, $id);
        
        $title = $News->getTitle();
        
        $em->remove($News);
        
        $em->flush();
        
        //dump($News); //die;
        
        return $this->render('news/delete.html.twig', [
            'title' => $title
        ]);
    }
    
}