<?php


namespace App\Controller; // app - папка src(с него идет поиск), по неймсейсу система понимает где искать файл

use \Symfony\Bundle\FrameworkBundle\Controller\AbstractController;// as AbstractController; //as можно не писать исполбзуется посл часть пути как имя

class HomeController extends AbstractController
{
    public function index()
    {
        return  $this->render('home.html.twig');//render функция ищет уже в папке template
    }
}