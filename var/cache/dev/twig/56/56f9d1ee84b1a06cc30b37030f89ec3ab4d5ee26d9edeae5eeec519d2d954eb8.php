<?php

/* news/show.html.twig */
class __TwigTemplate_613c588e7963b43aea40632f22dbddcf80cde7a0de6283ea1efb6c50da25fc3b extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "news/show.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "news/show.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "news/show.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "    <div class=\"row pt-3 pb-3\">
        <div class=\"col-9 ret-back\">
            <a href=\"";
        // line 6
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("newsList");
        echo "\"><i class=\"fa fa-arrow-left\" aria-hidden=\"true\"></i> News</a>
        </div>
        <div class=\"col-3\">
            <div class=\"data-news\">";
        // line 9
        echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["News"]) || array_key_exists("News", $context) ? $context["News"] : (function () { throw new Twig_Error_Runtime('Variable "News" does not exist.', 9, $this->source); })()), "dateUpdated", array()), "h:i d/m/Y"), "html", null, true);
        echo "</div>
        </div>
    </div>
    <h1 class=\"news-title\">";
        // line 12
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["News"]) || array_key_exists("News", $context) ? $context["News"] : (function () { throw new Twig_Error_Runtime('Variable "News" does not exist.', 12, $this->source); })()), "title", array()), "html", null, true);
        echo "</h1>
    <div class=\"news-desk pb-5\">
        <p>";
        // line 14
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["News"]) || array_key_exists("News", $context) ? $context["News"] : (function () { throw new Twig_Error_Runtime('Variable "News" does not exist.', 14, $this->source); })()), "description", array()), "html", null, true);
        echo "</p>
    </div>
    
    
    <button type=\"button\" class=\"btn btn-primary\" data-toggle=\"modal\" data-target=\"#deleteNews\">
        Delete news
    </button>
    <div class=\"modal fade\" id=\"deleteNews\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"deleteNewsLabel\" aria-hidden=\"true\">
        <div class=\"modal-dialog\" role=\"document\">
            <div class=\"modal-content\">
                <div class=\"modal-header\">
                    <h5 class=\"modal-title\" id=\"deleteNews\">Warning</h5>
                    <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\">
                        <span aria-hidden=\"true\">&times;</span>
                    </button>
                </div>
                <div class=\"modal-body\">
                    <p>Delete news:</p>
                    <b>";
        // line 32
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["News"]) || array_key_exists("News", $context) ? $context["News"] : (function () { throw new Twig_Error_Runtime('Variable "News" does not exist.', 32, $this->source); })()), "title", array()), "html", null, true);
        echo "</b>
                </div>
                <div class=\"modal-footer\">
                    <button type=\"button\" class=\"btn btn-secondary\" data-dismiss=\"modal\">No</button>
                    <a type=\"button\" class=\"btn btn-primary\" href=\"";
        // line 36
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("deleteNews", array("id" => twig_get_attribute($this->env, $this->source, (isset($context["News"]) || array_key_exists("News", $context) ? $context["News"] : (function () { throw new Twig_Error_Runtime('Variable "News" does not exist.', 36, $this->source); })()), "id", array()))), "html", null, true);
        echo "\">Delete</a>
                </div>
            </div>
        </div>
    </div>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "news/show.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  102 => 36,  95 => 32,  74 => 14,  69 => 12,  63 => 9,  57 => 6,  53 => 4,  44 => 3,  15 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'base.html.twig' %}

{% block body %}
    <div class=\"row pt-3 pb-3\">
        <div class=\"col-9 ret-back\">
            <a href=\"{{path('newsList')}}\"><i class=\"fa fa-arrow-left\" aria-hidden=\"true\"></i> News</a>
        </div>
        <div class=\"col-3\">
            <div class=\"data-news\">{{News.dateUpdated|date(\"h:i d/m/Y\")}}</div>
        </div>
    </div>
    <h1 class=\"news-title\">{{News.title}}</h1>
    <div class=\"news-desk pb-5\">
        <p>{{News.description}}</p>
    </div>
    
    
    <button type=\"button\" class=\"btn btn-primary\" data-toggle=\"modal\" data-target=\"#deleteNews\">
        Delete news
    </button>
    <div class=\"modal fade\" id=\"deleteNews\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"deleteNewsLabel\" aria-hidden=\"true\">
        <div class=\"modal-dialog\" role=\"document\">
            <div class=\"modal-content\">
                <div class=\"modal-header\">
                    <h5 class=\"modal-title\" id=\"deleteNews\">Warning</h5>
                    <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\">
                        <span aria-hidden=\"true\">&times;</span>
                    </button>
                </div>
                <div class=\"modal-body\">
                    <p>Delete news:</p>
                    <b>{{News.title}}</b>
                </div>
                <div class=\"modal-footer\">
                    <button type=\"button\" class=\"btn btn-secondary\" data-dismiss=\"modal\">No</button>
                    <a type=\"button\" class=\"btn btn-primary\" href=\"{{path('deleteNews', {'id':News.id}) }}\">Delete</a>
                </div>
            </div>
        </div>
    </div>
{% endblock %}", "news/show.html.twig", "/media/skillup_student/928CA3648CA34199/Development/SkillUP/PHP/symphony_project/templates/news/show.html.twig");
    }
}
