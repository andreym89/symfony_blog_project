<?php

/* news/list.html.twig */
class __TwigTemplate_5674a93eda19df0f24d29d568a7f8ae3457271a6dbf84d0c5a4a0cbaa958fe96 extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "news/list.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "news/list.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "news/list.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "    <div class=\"d-flex align-items-center\">
    <h1>News list</h1>
    <a class=\"btn btn-primary ml-3\" href=\"";
        // line 6
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("addNews");
        echo "\">Add news</a>
    </div>
    <div class=\"news-list row\">
        ";
        // line 9
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["news_list"]) || array_key_exists("news_list", $context) ? $context["news_list"] : (function () { throw new Twig_Error_Runtime('Variable "news_list" does not exist.', 9, $this->source); })()));
        foreach ($context['_seq'] as $context["_key"] => $context["news"]) {
            // line 10
            echo "            <div class=\"col-sm-12\">
                <div class=\"row oneNews\">
                    <h3 class=\"col-sm-9 mb-3 mt-3\">";
            // line 12
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["news"], "title", array()), "html", null, true);
            echo "</h3>
                    <div class=\"col-sm-3 mt-3 data-news\">";
            // line 13
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, $context["news"], "dateUpdated", array()), "h:i d/m/Y"), "html", null, true);
            echo "</div>
                    <div class=\"col-sm-12 news\">
                        ";
            // line 15
            echo twig_escape_filter($this->env, twig_slice($this->env, twig_get_attribute($this->env, $this->source, $context["news"], "description", array()), 0, 350), "html", null, true);
            echo "...
                        <a href=\"";
            // line 16
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("showNews", array("id" => twig_get_attribute($this->env, $this->source, $context["news"], "id", array()))), "html", null, true);
            echo "\" class=\"\">Read more</a>
                    </div>
                </div>
            </div>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['news'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 21
        echo "    </div>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "news/list.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  95 => 21,  84 => 16,  80 => 15,  75 => 13,  71 => 12,  67 => 10,  63 => 9,  57 => 6,  53 => 4,  44 => 3,  15 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'base.html.twig' %}

{% block body %}
    <div class=\"d-flex align-items-center\">
    <h1>News list</h1>
    <a class=\"btn btn-primary ml-3\" href=\"{{path('addNews')}}\">Add news</a>
    </div>
    <div class=\"news-list row\">
        {% for news in news_list %}
            <div class=\"col-sm-12\">
                <div class=\"row oneNews\">
                    <h3 class=\"col-sm-9 mb-3 mt-3\">{{news.title}}</h3>
                    <div class=\"col-sm-3 mt-3 data-news\">{{news.dateUpdated|date(\"h:i d/m/Y\")}}</div>
                    <div class=\"col-sm-12 news\">
                        {{news.description|slice(0, 350)}}...
                        <a href=\"{{path('showNews', {'id':news.id}) }}\" class=\"\">Read more</a>
                    </div>
                </div>
            </div>
        {% endfor %}
    </div>
{% endblock %}
", "news/list.html.twig", "/media/skillup_student/928CA3648CA34199/Development/SkillUP/PHP/symphony_project/templates/news/list.html.twig");
    }
}
