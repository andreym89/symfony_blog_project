<?php

/* contacts.html.twig */
class __TwigTemplate_1047e9cc655427b8c86068ba4d404339e859a3df317afde88040e20639668eaf extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "contacts.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "contacts.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "contacts.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "    <div class=\"row justify-content-center\">
        <div class=\"col-12 col-md-8 col-lg-6 pb-5\">
            <form action=\"mail.php\" method=\"post\">
                <div class=\"card border-primary main-radius\">
                    <div class=\"card-header p-0 contact-form\">
                        <div class=\"bg-info text-white text-center py-3 contact-form\">
                            <h3><i class=\"fa fa-envelope\"></i> Contact form</h3>
                        </div>
                    </div>
                    <div class=\"card-body p-3\">
                        <div class=\"form-group\">
                            <div class=\"input-group mb-2\">
                                <div class=\"input-group-prepend\">
                                    <div class=\"input-group-text\"><i class=\"fa fa-user text-info\"></i></div>
                                </div>
                                <input type=\"text\" class=\"form-control\" id=\"nombre\" name=\"nombre\" placeholder=\"Name\" required>
                            </div>
                        </div>
                        <div class=\"form-group\">
                            <div class=\"input-group mb-2\">
                                <div class=\"input-group-prepend\">
                                    <div class=\"input-group-text\"><i class=\"fa fa-envelope text-info\"></i></div>
                                </div>
                                <input type=\"email\" class=\"form-control\" id=\"nombre\" name=\"email\" placeholder=\"Email\" required>
                            </div>
                        </div>
                        <div class=\"form-group\">
                            <div class=\"input-group mb-2\">
                                <div class=\"input-group-prepend\">
                                    <div class=\"input-group-text\"><i class=\"fa fa-comment text-info\"></i></div>
                                </div>
                                <textarea class=\"form-control\" placeholder=\"Message\" required></textarea>
                            </div>
                        </div>
                        <div class=\"text-center\">
                            <input type=\"submit\" value=\"Send\" class=\"btn btn-info btn-block py-2 main-radius\">
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "contacts.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  53 => 4,  44 => 3,  15 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'base.html.twig' %}

{% block body %}
    <div class=\"row justify-content-center\">
        <div class=\"col-12 col-md-8 col-lg-6 pb-5\">
            <form action=\"mail.php\" method=\"post\">
                <div class=\"card border-primary main-radius\">
                    <div class=\"card-header p-0 contact-form\">
                        <div class=\"bg-info text-white text-center py-3 contact-form\">
                            <h3><i class=\"fa fa-envelope\"></i> Contact form</h3>
                        </div>
                    </div>
                    <div class=\"card-body p-3\">
                        <div class=\"form-group\">
                            <div class=\"input-group mb-2\">
                                <div class=\"input-group-prepend\">
                                    <div class=\"input-group-text\"><i class=\"fa fa-user text-info\"></i></div>
                                </div>
                                <input type=\"text\" class=\"form-control\" id=\"nombre\" name=\"nombre\" placeholder=\"Name\" required>
                            </div>
                        </div>
                        <div class=\"form-group\">
                            <div class=\"input-group mb-2\">
                                <div class=\"input-group-prepend\">
                                    <div class=\"input-group-text\"><i class=\"fa fa-envelope text-info\"></i></div>
                                </div>
                                <input type=\"email\" class=\"form-control\" id=\"nombre\" name=\"email\" placeholder=\"Email\" required>
                            </div>
                        </div>
                        <div class=\"form-group\">
                            <div class=\"input-group mb-2\">
                                <div class=\"input-group-prepend\">
                                    <div class=\"input-group-text\"><i class=\"fa fa-comment text-info\"></i></div>
                                </div>
                                <textarea class=\"form-control\" placeholder=\"Message\" required></textarea>
                            </div>
                        </div>
                        <div class=\"text-center\">
                            <input type=\"submit\" value=\"Send\" class=\"btn btn-info btn-block py-2 main-radius\">
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
{% endblock %}
", "contacts.html.twig", "/media/andrew/928CA3648CA34199/Development/SkillUP/PHP/symphony_project/templates/contacts.html.twig");
    }
}
